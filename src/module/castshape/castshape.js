import { ARS } from '../config.js';
import * as utilitiesManager from "../utilities.js";
import * as effectManager from "../effect/effects.js";
import { CombatManager } from "../combat/combat.js";
import * as dialogManager from "../dialog.js";
import * as debug from "../debug.js";


/**
 * A helper class for building MeasuredTemplates for actions
 */
class AbilityTemplate extends MeasuredTemplate {

  /**
   * Track the timestamp when the last mouse move event was captured.
   * @type {number}
   */
  #moveTime = 0;

  /* -------------------------------------------- */

  /**
   * The initially active CanvasLayer to re-activate after the workflow is complete.
   * @type {CanvasLayer}
   */
  #initialLayer;

  /* -------------------------------------------- */

  /**
   * Track the bound event handlers so they can be properly canceled later.
   * @type {object}
   */
  #events;

  /* -------------------------------------------- */

  /**
   * A factory method to create an AbilityTemplate instance using provided data from an Item instance
   * @param {Item} item               The Item object for which to construct the template
   * @returns {AbilityTemplate|null}    The template object, or null if the item does not produce a template
   */

  static fromItem(item, templateData) {
    const target = item.target ?? {};

    // Return the template constructed from the item data
    const cls = CONFIG.MeasuredTemplate.documentClass;
    const template = new cls(templateData, { parent: canvas.scene });
    const object = new this(template);
    object.item = item;
    object.actorSheet = item.actor?.sheet || null;
    return object;
  }

  /* -------------------------------------------- */

  /**
   * Creates a preview of the spell template.
   * @returns {Promise}  A promise that resolves with the final measured template if created.
   */
  drawPreview() {
    const initialLayer = canvas.activeLayer;

    // Draw the template and switch to the template layer
    this.draw();
    this.layer.activate();
    this.layer.preview.addChild(this);

    // Hide the sheet that originated the preview
    this.actorSheet?.minimize();

    // Activate interactivity
    return this.activatePreviewListeners(initialLayer);
  }

  /* -------------------------------------------- */

  /**
   * Activate listeners for the template preview
   * @param {CanvasLayer} initialLayer  The initially active CanvasLayer to re-activate after the workflow is complete
   * @returns {Promise}                 A promise that resolves with the final measured template if created.
   */
  activatePreviewListeners(initialLayer) {
    return new Promise((resolve, reject) => {
      this.#initialLayer = initialLayer;
      this.#events = {
        cancel: this._onCancelPlacement.bind(this),
        confirm: this._onConfirmPlacement.bind(this),
        move: this._onMovePlacement.bind(this),
        resolve,
        reject,
        rotate: this._onRotatePlacement.bind(this)
      };

      // Activate listeners
      canvas.stage.on("mousemove", this.#events.move);
      canvas.stage.on("mousedown", this.#events.confirm);
      canvas.app.view.oncontextmenu = this.#events.cancel;
      canvas.app.view.onwheel = this.#events.rotate;
    });
  }

  /* -------------------------------------------- */

  /**
   * Shared code for when template placement ends by being confirmed or canceled.
   * @param {Event} event  Triggering event that ended the placement.
   */
  async _finishPlacement(event) {
    this.layer._onDragLeftCancel(event);
    canvas.stage.off("mousemove", this.#events.move);
    canvas.stage.off("mousedown", this.#events.confirm);
    canvas.app.view.oncontextmenu = null;
    canvas.app.view.onwheel = null;
    this.#initialLayer.activate();
    await this.actorSheet?.maximize();
  }

  /* -------------------------------------------- */

  /**
   * Move the template preview when the mouse moves.
   * @param {Event} event  Triggering mouse event.
   */
  _onMovePlacement(event) {
    event.stopPropagation();
    const now = Date.now(); // Apply a 20ms throttle
    if (now - this.#moveTime <= 20) return;
    const center = event.data.getLocalPosition(this.layer);
    const interval = canvas.grid.type === CONST.GRID_TYPES.GRIDLESS ? 0 : 2;
    const snapped = canvas.grid.getSnappedPosition(center.x, center.y, interval);
    const defaultOutOfRangeFillColor = "#3f0607";
    const defaultFillColor = this.document.flags.fillColor;

    this.document.updateSource({ x: snapped.x, y: snapped.y });

    let objectDistance = 0;

    if(this.document.t == 'rect')
	  {		  
		  objectDistance = Math.sqrt(Math.pow(this.document.flags.actorOrigin_x - (snapped.x+((Math.cos(this.document.direction*Math.PI/180)*this.document.distance*canvas.grid.grid.w/canvas.dimensions.distance)/2)),2)+Math.pow(this.document.flags.actorOrigin_y - (snapped.y+((Math.sin(this.document.direction*Math.PI/180)*this.document.distance*canvas.grid.grid.w/canvas.dimensions.distance)/2)),2))/canvas.grid.grid.w*canvas.dimensions.distance;
	  }

    else if(this.document.flags.orientCenter == true)
    {
      objectDistance = Math.sqrt(Math.pow(this.document.flags.actorOrigin_x - (snapped.x+((Math.cos(this.document.direction*Math.PI/180)*this.document.distance*canvas.grid.grid.w/canvas.dimensions.distance)/2)),2)+Math.pow(this.document.flags.actorOrigin_y - (snapped.y+((Math.sin(this.document.direction*Math.PI/180)*this.document.distance*canvas.grid.grid.w/canvas.dimensions.distance)/2)),2))/canvas.grid.grid.w*canvas.dimensions.distance;
    }

    else
	  {
      objectDistance = Math.sqrt(Math.pow(this.document.flags.actorOrigin_x - snapped.x,2)+Math.pow(this.document.flags.actorOrigin_y - snapped.y,2))/canvas.grid.grid.w*canvas.dimensions.distance;
	  }   

    // Change color of template shape if placement exceeds the range of the spell
    if (objectDistance > this.document.flags.spellrange) {
      this.document.updateSource({ fillColor: defaultOutOfRangeFillColor });
    }
    else { this.document.updateSource({ fillColor: defaultFillColor }); }    

    this.refresh();
    this.#moveTime = now;
  }

  /* -------------------------------------------- */

  /**
   * Rotate the template preview by 3˚ increments when the mouse wheel is rotated.
   * @param {Event} event  Triggering mouse event.
   */
  _onRotatePlacement(event) {
    if (event.ctrlKey) event.preventDefault(); // Avoid zooming the browser window
    event.stopPropagation();
    const delta = canvas.grid.type > CONST.GRID_TYPES.SQUARE ? 30 : 15;
    const snap = event.shiftKey ? delta : 5;
    const update = { direction: this.document.direction + (snap * Math.sign(event.deltaY)) };
    this.document.updateSource(update);
    this.refresh();
  }

  /* -------------------------------------------- */

  /**
   * Confirm placement when the left mouse button is clicked.
   * @param {Event} event  Triggering mouse event.
   */
  async _onConfirmPlacement(event) {
    await this._finishPlacement(event);
    const interval = canvas.grid.type === CONST.GRID_TYPES.GRIDLESS ? 0 : 2;
    const destination = canvas.grid.getSnappedPosition(this.document.x, this.document.y, interval);
    this.document.updateSource(destination);
    this.#events.resolve(canvas.scene.createEmbeddedDocuments("MeasuredTemplate", [this.document.toObject()]));
  }

  /* -------------------------------------------- */

  /**
   * Cancel placement when the right mouse button is clicked.
   * @param {Event} event  Triggering mouse event.
   */
  async _onCancelPlacement(event) {
    await this._finishPlacement(event);
    this.#events.reject();
  }

}

export class PlaceCastShape {

  static async evaluateFormula(string, Object) {
    return utilitiesManager.evaluateFormulaValueAsync(string, Object.getRollData());
  }

  static async placeCastShape(dd) {
    const templateData = {
      t: null,
      x: 0,
      y: 0,
      distance: 0,
      fillColor: dd.action.castShapeProperties.fillColor,
      flags: { spellrange: 0, actorOrigin_x: 0, actorOrigin_y: 0, orientCenter: false, fillColor: dd.action.castShapeProperties.fillColor }
    };
    // Try catch if there is some condition that creates an actor without x,y co-ords.
    try {
      templateData.flags.actorOrigin_x = Number(dd.data.sourceToken.document.x + (dd.data.sourceToken.document.width * canvas.grid.grid.w / 2));
    } catch (err) { console.log("Actor origin missing", err); }
    try {
      templateData.flags.actorOrigin_y = Number(dd.data.sourceToken.document.y + (dd.data.sourceToken.document.height * canvas.grid.grid.w / 2));
    } catch (err) { console.log("Actor origin missing", err); }

    switch (dd.action.targetShape.type) {
      case "circle":
        let circleRadius = await PlaceCastShape.evaluateFormula(dd.action.castShapeProperties.castShapeRadius.formula, dd.source);
        let circlecastRange = await PlaceCastShape.evaluateFormula(dd.action.castShapeProperties.range.formula, dd.source);
        templateData.t = 'circle';
        templateData.distance = circleRadius;
        templateData.flags.spellrange = circlecastRange;
        break;

      case "ray":
        let rayLength = await PlaceCastShape.evaluateFormula(dd.action.castShapeProperties.castShapeLength.formula, dd.source);
        let rayWidth = await PlaceCastShape.evaluateFormula(dd.action.castShapeProperties.castShapeWidth.formula, dd.source);
        let raycastRange = await PlaceCastShape.evaluateFormula(dd.action.castShapeProperties.range.formula, dd.source);
        templateData.t = 'ray';
        templateData.distance = rayLength;
        templateData.width = rayWidth;
        templateData.flags.spellrange = raycastRange;
        break;

      
      case "ray2": // used for creating a rectangle that rotates around the middle cursor
        let ray2Length = await PlaceCastShape.evaluateFormula(dd.action.castShapeProperties.castShapeLength.formula, dd.source);
        let ray2Width = await PlaceCastShape.evaluateFormula(dd.action.castShapeProperties.castShapeWidth.formula, dd.source);
        let ray2castRange = await PlaceCastShape.evaluateFormula(dd.action.castShapeProperties.range.formula, dd.source);
        templateData.t = 'ray';
          templateData.distance = ray2Length;
          templateData.width = ray2Width;
          templateData.flags.spellrange = ray2castRange;
          templateData.flags.orientCenter = true;
        break;  

      case "rectangle":
        let rectangleLength = await PlaceCastShape.evaluateFormula(dd.action.castShapeProperties.castShapeLength.formula, dd.source);
        let rectangleWidth = await PlaceCastShape.evaluateFormula(dd.action.castShapeProperties.castShapeWidth.formula, dd.source);
        let rectanglecastRange = await PlaceCastShape.evaluateFormula(dd.action.castShapeProperties.range.formula, dd.source);
        templateData.t = 'rect';
        templateData.distance = Math.sqrt(rectangleWidth * rectangleWidth + rectangleLength * rectangleLength);
        templateData.flags.spellrange = rectanglecastRange;
        try {
          templateData.direction = Math.atan(rectangleWidth / rectangleLength) * 180 / Math.PI;
        } catch (err) { console.log("Place Cast shape error in calculation for direction angle"); templateData.direction = 90 }
        break;

      case "cone":
        let coneAngle = await PlaceCastShape.evaluateFormula(dd.action.castShapeProperties.castShapeAngle.formula, dd.source);
        let coneLength = await PlaceCastShape.evaluateFormula(dd.action.castShapeProperties.castShapeLength.formula, dd.source);
        let conecastRange = await PlaceCastShape.evaluateFormula(dd.action.castShapeProperties.range.formula, dd.source);
        templateData.t = 'cone';
        templateData.distance = coneLength;
        templateData.angle = coneAngle;
        templateData.flags.spellrange = conecastRange;
        break;
        
      case "default":
        console.log("No selected shape");
        break;
    }
    // XX
    let templates;
    try {
      templates = await (AbilityTemplate.fromItem(dd, templateData))?.drawPreview();
    } catch (err) { console.log(err); }
    console.log("castshape.js", { templates })
  }
}
